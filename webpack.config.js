const webpack = require('webpack'); //to access built in plugins
const path = require('path');


module.exports = {
    entry: './src/js/app.js',

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        loaders:[
            {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {test: /\.css$/, loader: "style-loader!css-loader"},
            {test: /\.js$/, loader: "babel-loader", exclude: /node_modules/, query: {presets:['es2015']}}
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin()
    ]
};

// if(process.env.NODE_ENV === 'production') {
//     module.exports.plugins.push(
//         new webpack.optimize.UglifyJsPlugin()
//     );
// }